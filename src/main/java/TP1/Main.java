package TP1;

import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import TP1.ASD.Subject;
import TP1.ASD.TurtleObjectEntity;

import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;


public class Main {
  public static void main(String[] args) {
      /*
    // Use with a manually made AST
	  
	LinkedList<ASD.Subject> subjects = new LinkedList<ASD.Subject>(); //TODO
	  
	ASD.Name poly117 = new ASD.Name("poly117"); ASD.Entity poly117E = new ASD.Entity(poly117);
	ASD.Name type = new ASD.Name("type"); ASD.Entity typeE = new ASD.Entity(type);
	ASD.Name poly = new ASD.Name("poly"); ASD.Entity polyE = new ASD.Entity(poly);
	ASD.Name auteur = new ASD.Name("auteur"); ASD.Entity auteurE = new ASD.Entity(auteur);
	ASD.Name ridoux = new ASD.Name("Ridoux"); ASD.Entity ridouxE = new ASD.Entity(ridoux);
	ASD.Name ferre = new ASD.Name("Ferre"); ASD.Entity ferreE = new ASD.Entity(ferre);
	ASD.Name titre = new ASD.Name("titre"); ASD.Entity titreE = new ASD.Entity(titre);
	ASD.Name compilation = new ASD.Name("Compilation");
	ASD.Name personne = new ASD.Name("personne"); ASD.Entity personneE = new ASD.Entity(personne);
	ASD.Name professeur = new ASD.Name("professeur"); ASD.Entity professeurE = new ASD.Entity(professeur);
	
	ASD.TurtleObjectEntity ridouxO = new ASD.TurtleObjectEntity(ridouxE);
	ASD.TurtleObjectEntity ferreO = new ASD.TurtleObjectEntity(ferreE);
	
	ASD.Predicate auteurP = new ASD.Predicate(auteurE);
	LinkedList<ASD.TurtleObject> ridouxFerre = new LinkedList<ASD.TurtleObject>(); 
	ridouxFerre.add(ridouxO); ridouxFerre.add(ferreO);
	
	ASD.Predicate_Objects_Couple auteurPOC = new ASD.Predicate_Objects_Couple(auteurP, ridouxFerre);
	LinkedList<ASD.Predicate_Objects_Couple> polyPOCs = new LinkedList<ASD.Predicate_Objects_Couple>();
	polyPOCs.add(auteurPOC);
	
	ASD.Subject polyS = new ASD.Subject(poly117E, polyPOCs);
	subjects.add(polyS);
	
	
    ASD.Document ast = new ASD.Document(subjects);
    System.out.println(ast.toNtriples());

    */

    // Use with lexer and parser
    
    try {
      // Set input
      CharStream input;
      if(args.length == 0) {
        // From standard input
        input = CharStreams.fromStream(System.in);
      }
      else {
        // From file set in first argument
        input = CharStreams.fromPath(Paths.get(args[0]));
      }

      // Instantiate Lexer
      TurtleLexer lexer = new TurtleLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);

      // Instantiate Parser
      TurtleParser parser = new TurtleParser(tokens);

      // Parse
      ASD.Document ast = parser.document().out;

      // Print as Ntriples
      System.out.println(ast.toNtriples());
    } catch(IOException e) {
      e.printStackTrace();
    }
    
  }
}
