package TP1;

import java.util.List;
import java.util.ListIterator;

public class ASD {
  static public class Document {
    // Fill here!
	List<Subject> subjects;

	public Document(List<Subject> subjects) {
		this.subjects = subjects;
	}

    public String toNtriples() {
    	String nTriplesDisplay = "";

    	ListIterator<Subject> subject_iterator = this.subjects.listIterator();

    	while(subject_iterator.hasNext()) {
    		Subject current_subject = subject_iterator.next();
    		nTriplesDisplay += current_subject.toNTriples();
    	}

    	return nTriplesDisplay;
    }


  }

  /***/

  static public class Subject {
	Entity entity;
	List<Predicate_Objects_Couple> predicate_objects_couple;

	public Subject(Entity entity, List<Predicate_Objects_Couple> predicate_objects_couple) {
		this.entity = entity;
		this.predicate_objects_couple = predicate_objects_couple;
	}

	public String toNTriples() {
		// TODO Auto-generated method stub
		String nTriplesDisplay = "";

    	ListIterator<Predicate_Objects_Couple> poCouple_iterator = this.predicate_objects_couple.listIterator();

    	while(poCouple_iterator.hasNext()) {
    		Predicate_Objects_Couple current_poCouple = poCouple_iterator.next();
    		String subjectName = this.entity.toString();
    		nTriplesDisplay += current_poCouple.toNTriples(subjectName);
    	}

    	return nTriplesDisplay;
	}
  }

  /***/

  static public class Predicate_Objects_Couple{
	  Predicate predicate;
	  List<TurtleObject> objects_list;

	  public Predicate_Objects_Couple(Predicate predicate, List<TurtleObject> objects_list) {
		  this.predicate = predicate;
		  this.objects_list = objects_list;
	  }

	public String toNTriples(String subjectName) {
		// TODO Auto-generated method stub

		String nTriplesDisplay = "";

    	ListIterator<TurtleObject> objects_iterator = this.objects_list.listIterator();

    	while(objects_iterator.hasNext()) {
    		TurtleObject current_object = objects_iterator.next();
    		String predicateName = this.predicate.toString();
    		nTriplesDisplay += 	subjectName 	+ " "
    						+  	predicateName	+ " "
    						+ 	current_object.toString() + " .\n";
    	}

    	return nTriplesDisplay;
	}

  }

  /***/

  static public class Predicate{
	  Entity entity;

	  public Predicate(Entity e) {
		  this.entity = e;
	  }

	  public String toString() {
		  return this.entity.toString();
	  }
  }

  /***/

  static public abstract class TurtleObject {
	  public abstract String toString();
  }

  static public class TurtleObjectEntity extends TurtleObject {
	  Entity entity;

	  public TurtleObjectEntity(Entity e) {
		  this.entity = e;
	  }

	  @Override
	  public String toString() {
		  return this.entity.toString();
	  }
  }

  static public class TurtleName extends TurtleObject{
	  Name name;

	  public TurtleName(Name n) {
		  this.name = n;
	  }

	  @Override
	  public String toString() {
		  return '"'+this.name.toString()+'"';
	  }
  }
  /***/

  static public class Entity{
	  Name name;

	  public Entity(Name n) {
		  this.name = n;
	  }

	  public String toString() {
		  return "<"+this.name.toString()+">";
	  }
  }

  static public class Name{
	  String string;

	  public Name(String s) {
		  this.string = s;
	  }

	  public String toString() {
		  return this.string;
	  }
  }

  // Fill here!
}
