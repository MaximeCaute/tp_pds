parser grammar TurtleParser;

options {
  language = Java;
  tokenVocab = TurtleLexer;
}

@header {
  package TP1;

  import java.util.LinkedList;
  import java.util.List;
}

document returns [ASD.Document out]
  : s=statements EOF { $out = new ASD.Document($s.out); }
  ;

// Fill here!

statements returns [List<ASD.Subject> out]
  : stt=statement stts=statements {$out = $stts.out; $out.add($stt.out);}
    | stt=statement { $out = new LinkedList<ASD.Subject>();
                      $out.add($stt.out);}

  ;

statement returns [ASD.Subject out]
  : sub=entity poc=predicate_objects_couples END_STT {$out = new ASD.Subject($sub.out, $poc.out); }
  ;

predicate_objects_couples returns [List<ASD.Predicate_Objects_Couple> out]
  : poc=predicate_objects_couple DELIM_POC pocs=predicate_objects_couples
                                                      {$out=$pocs.out; $out.add($poc.out);}
    | poc=predicate_objects_couple { $out = new LinkedList<ASD.Predicate_Objects_Couple>();
                                      $out.add($poc.out);}
  ;

predicate_objects_couple returns [ASD.Predicate_Objects_Couple out]
  : pred=predicate objs=objects {$out = new ASD.Predicate_Objects_Couple($pred.out, $objs.out);}
  ;

objects returns [List<ASD.TurtleObject> out]
  : obj=object DELIM_OBJ objs = objects {$out=$objs.out;$out.add($obj.out);}
    | obj=object { $out = new LinkedList<ASD.TurtleObject>();
                  $out.add($obj.out);}
  ;

object returns [ASD.TurtleObject out]
  : e=entity {$out = new ASD.TurtleObjectEntity($e.out);}
    | n=name {$out = new ASD.TurtleName($n.out);}
  ;

predicate returns [ASD.Predicate out]
  : e=entity {$out = new ASD.Predicate($e.out);}
  ;

entity returns [ASD.Entity out]
  : OPENING_BRACK_ENTITY ID CLOSING_BRACK_ENTITY {$out = new ASD.Entity(new ASD.Name($ID.text));}
  ;

name returns [ASD.Name out]
  : BRACK_NAME STR BRACK_NAME {$out = new ASD.Name($STR.text);}
  | BRACK_NAME ID BRACK_NAME {$out = new ASD.Name($ID.text);}
  ;
