lexer grammar TurtleLexer;

options {
  language = Java;
}

@header {
  package TP1;
}

// ignore whitespaces
WS : (' '|'\n'|'\t'|'\r'|'\u000C')+ -> skip
   ;



END_STT: ('.');

DELIM_POC: (';');

DELIM_OBJ: (',');

OPENING_BRACK_ENTITY: ('<');
CLOSING_BRACK_ENTITY: ('>');

BRACK_NAME: ('"');
//CLOSING_BRACK_NAME: ('"');

//ID: ('a'..'z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
ID: ('a'..'z'|'A'..'Z'|'0'..'9'|'_'|'-')+ ;
// Strings
STR: (WS|' '|ID|'&')* ;

fragment ASCII  : ~('\n'|'"'|'<'|'>');
