# PDS: TP1 - Un traducteur de RDF/Turtle vers RDF/Ntriples
Maxime Cauté & Rémi Piau

## How to

Just run `sh demo.sh` to compile and run tests with some comment

### Compile
`./gradlew build`

### Run
`java -jar build/libs/TP1.jar`
will make the program wait for input on stdin.

### Test
Test our programm with
`java -jar build/libs/TP1.jar < tests/test1.ttl`
`java -jar build/libs/TP1.jar < tests/test2.ttl`

---
## Notes
We do not handle errors well since it is not the point of the tp.
We have restricted valid characters to [a-zA-Z0-9] and [_|-|&| |\n|\t].

You can find the asd we used in file of the form `asd_*.txt`.
