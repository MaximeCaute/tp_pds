#!/bin/sh

echo "Compiling..."
./gradlew build

echo "<Press enter to continue>"
read TEST
echo "##############################################"
echo "Running test: tests/test1.ttl"
echo "Input is:"
cat tests/test1.ttl
echo "<Press enter to continue>"
read TEST
echo "Output is:"
java -jar build/libs/TP1.jar < tests/test1.ttl
echo "<Press enter to continue>"
read TEST

echo "##############################################"
echo "Running test: tests/test2.ttl"
echo "Input is:"
cat tests/test2.ttl
echo "<Press enter to continue>"
read TEST
echo "Output is:"
java -jar build/libs/TP1.jar < tests/test2.ttl
